export class Clima{
    public ciudad:string;
    public urlImage:string;
    public temperatura:number;
    public humedad:number;
    public tempMaxima:number;
    public tempMinima:number;
    public porcentaje:number;

    constructor(
        public Ciudad:string,
        public UrlImage:string,
        public Temperatura:number,
        public Humedad:number,
        public TempMaxima:number,
        public TempMinima:number,
        public Porcentaje:number
    ){
        this.ciudad=Ciudad;
        this.urlImage = `http://openweathermap.org/img/wn/${UrlImage}@2x.png`;
        this.temperatura = Temperatura;
        this.humedad = Humedad;
        this.tempMaxima = TempMaxima;
        this.tempMinima = TempMinima;
        this.porcentaje = Porcentaje


    }

}