import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class ClimaService {

  apiURL: string = 'http://api.openweathermap.org/data/2.5/weather?APPID=2ebb8d634e6ad8b6841eb487bcd37103&id=';

  constructor(private httpClient: HttpClient ) { }

  getClima(idCiudad:string){

    return this.httpClient.get(`${this.apiURL}${idCiudad}`);
  }

}
