import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, FormControl, ValidatorFn } from '@angular/forms';
import { ClimaService } from '../../services/clima.service';
import { Clima } from 'src/app/model/clima';


@Component({
  selector: 'app-clima',
  templateUrl: './clima.component.html',
  styleUrls: ['./clima.component.css']
})
export class ClimaComponent  {

  form: FormGroup;


  Ciudades:any = [
    { value: '3601783', text: 'San Pedro Sula'},
    { value: '2643743', text: 'Londres'},
    { value: '2968815', text: 'Paris'},
    { value: '5128638', text: 'Nueva York'},
    { value: '1850147', text: 'Tokyo'},
    { value: '2147714', text: 'Sydney'},
    { value: '3435910', text: 'Buenos Aires'},
    { value: '3451190', text: 'Rio de Janeiro'},
    { value: '2950158', text: 'Berlín'},
    { value: '5601538', text: 'Moscú'},
    { value: '292223', text: 'Dubai'},
  ];

  CiudadSelecionada:any;

  clima:Clima;

  constructor(private climaService : ClimaService) {

    this.datosClima('3601783');
   }

 /* ngOnInit() {
  }*/


  CargarClima(event){
    
    this.datosClima(event);

  }

  datosClima(idCiudad:string){
      this.climaService.getClima(idCiudad).subscribe( (data:any) =>{
      console.log(data);
      this.clima = new Clima(
        data.name,
        data.weather[0].icon,
        data.main.temp,
        data.main.humidity,
        data.main.temp_max,
        data.main.temp_min,
        data.clouds.all
      );

      console.log(this.clima);

  });

  }

}
